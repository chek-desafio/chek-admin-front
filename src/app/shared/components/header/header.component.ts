import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  @Output() changeOpened = new EventEmitter<boolean>();

  // get usuario() {
  //   return this.authService.usuario;
  // }
  constructor() {}

  changeOpenedValue() {
    this.changeOpened.emit();
  }
  ngOnInit(): void {
    // console.log('USUARIO => ', this.authService.usuario);
  }
}
