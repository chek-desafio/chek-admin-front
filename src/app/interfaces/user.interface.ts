// Generated by https://quicktype.io

export interface UserResponse {
    ok:   boolean;
    user: User;
}

export interface User {
    _id:     string;
    balance: number;
    name:    string;
    mail:    string;
    rut:     string;
    status:  boolean;
    role:    string;
}


// Generated by https://quicktype.io

export interface AllUsersResponse {
    ok:    boolean;
    users: Users[];
}

export interface Users {
    _id:                string;
    balance?:           number;
    name:               string;
    mail:               string;
    rut:                string;
    status:             boolean;
    createdAt:          string;
    updatedAt:          string;
    tokenPurpose?:      string;
    verificationToken?: string;
    role:               string;
}
