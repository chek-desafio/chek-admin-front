import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AllUsersResponse, UserResponse } from '../interfaces/user.interface';
import { Observable } from 'rxjs';
import { getToken } from './utils';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}
  private baseUrl: string = environment.baseUrl;

  getUsers(): Observable<AllUsersResponse> {
    return this.http.get<AllUsersResponse>(`${this.baseUrl}/user`, {
      headers: getToken(),
    });
  }
}
