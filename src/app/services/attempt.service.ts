import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IPInfo, LoginAttempt } from '../interfaces/attempts.interface';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { getToken } from './utils';

@Injectable({
  providedIn: 'root',
})
export class AttemptService {
  constructor(private http: HttpClient) {}
  private baseUrl: string = environment.baseUrl;

  getAttempts(): Observable<LoginAttempt> {
    return this.http.get<LoginAttempt>(`${this.baseUrl}/attempt`, {
      headers: getToken(),
    });
  }

  newLoginAttempt(
    mail: string,
    ipAddress: string,
    location: string,
    country: string
  ): Observable<LoginAttempt> {
    return this.http.post<LoginAttempt>(`${this.baseUrl}/attempt`, {
      mail,
      ipAddress,
      location,
      country,
    });
  }

  getUserIp(): Observable<IPInfo> {
    return this.http.get<IPInfo>('https://ipapi.co/json/');
  }
}
