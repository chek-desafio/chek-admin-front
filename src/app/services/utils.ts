import { HttpHeaders } from '@angular/common/http';

export const getToken = () => {
  return new HttpHeaders().set('token', localStorage.getItem('token') || '');
};
