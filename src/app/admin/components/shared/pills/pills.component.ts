import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-pills',
  templateUrl: './pills.component.html',
  styleUrls: ['./pills.component.css'],
})
export class PillsComponent implements OnInit {
  @Input() data!: string;
  @Input() isStatus!: boolean;
  @Input() isClient!: boolean;

  constructor() {}

  ngOnInit(): void {}
}
