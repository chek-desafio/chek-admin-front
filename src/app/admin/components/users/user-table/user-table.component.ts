import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { User, Users } from 'src/app/interfaces/user.interface';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css'],
})
export class UserTableComponent implements OnInit {
  constructor(private userService: UserService) {}

  displayedColumns: string[] = [
    'name',
    'mail',
    'status',
    'createdAt',
    'rut',
    'role',
  ];
  dataSource!: MatTableDataSource<Users>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  userResponse!: Users[];
  isLoading: boolean = true;

  ngOnInit(): void {
    this.userService.getUsers().subscribe((resp) => {
      this.userResponse = resp.users;
      this.dataSource = new MatTableDataSource(this.userResponse);

      setTimeout(() => (this.dataSource.paginator = this.paginator));
      setTimeout(() => (this.dataSource.sort = this.sort));

      this.isLoading = false;
    });
  }
}
