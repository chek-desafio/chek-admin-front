import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Attempt } from 'src/app/interfaces/attempts.interface';
import { AttemptService } from 'src/app/services/attempt.service';

@Component({
  selector: 'app-dashboard-table',
  templateUrl: './dashboard-table.component.html',
  styleUrls: ['./dashboard-table.component.css'],
})
export class DashboardTableComponent implements OnInit {
  constructor(private attemptService: AttemptService) {}

  displayedColumns: string[] = [
    'name',
    'mail',
    'ipAddress',
    'browser',
    'location',
    'country',
    'createdAt',
  ];
  dataSource!: MatTableDataSource<Attempt>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  attemptsResponse!: Attempt[];
  isLoading: boolean = true;
  ngOnInit(): void {
    this.attemptService.getAttempts().subscribe((resp) => {
      this.attemptsResponse = resp.attempts;
      console.log(this.attemptsResponse);
      this.dataSource = new MatTableDataSource(this.attemptsResponse);

      setTimeout(() => (this.dataSource.paginator = this.paginator));
      setTimeout(() => (this.dataSource.sort = this.sort));

      this.isLoading = false;
    });
  }
}
