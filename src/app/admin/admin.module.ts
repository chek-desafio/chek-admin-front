import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PanelComponent } from './components/panel/panel.component';
import { UsersComponent } from './components/users/users.component';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';

import { SharedModule } from '../shared/shared.module';
import { UserTableComponent } from './components/users/user-table/user-table.component';
import { DashboardTableComponent } from './components/dashboard/dashboard-table/dashboard-table.component';
import { PillsComponent } from './components/shared/pills/pills.component';

@NgModule({
  declarations: [
    DashboardComponent,
    PanelComponent,
    UsersComponent,
    UserTableComponent,
    DashboardTableComponent,
    PillsComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MatSidenavModule,
    SharedModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
  ],
})
export class AdminModule {}
