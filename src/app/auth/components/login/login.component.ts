import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthResponse, CustomError } from 'src/app/interfaces/auth.interface';
import { AttemptService } from 'src/app/services/attempt.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private loginAttemptsService: AttemptService,
    private titleService: Title,
    private _snackBar: MatSnackBar
  ) {}

  loginForm: FormGroup = this.fb.group({
    mail: [
      '',
      [
        Validators.required,
        Validators.pattern(
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        ),
      ],
    ],
    password: ['', [Validators.required, Validators.minLength(6)]],
  });
  clickButton: boolean = false;
  isLoading: boolean = false;

  ngOnInit(): void {
    this.titleService.setTitle('Administrador Chek - Inicia sesión');
  }

  login(): void {
    if (!this.loginForm.valid) return;
    this.isLoading = true;
    this.clickButton = true;
    this.authService
      .login(
        this.loginForm.get('mail')?.value,
        this.loginForm.get('password')?.value
      )
      .subscribe((resp: AuthResponse | CustomError) => {
        if ('error' in resp) {
          this.handleLoginError(resp);
        } else {
          this.handleLoginSuccess(resp);
        }
      });
  }

  handleLoginSuccess(resp: AuthResponse): void {
    if (resp.user.role === 'Admin') {
      this.router.navigateByUrl('/admin/dashboard');
    } else if (resp.user.role === 'Client') {
      this.openSnackBar(
        'Debe ingresar desde el panel de administración ',
        'OK'
      );
      this.loginForm.reset();
      this.isLoading = false;
    }
  }

  handleLoginError(errorInfo: CustomError): void {
    console.log(errorInfo.message);
    if (errorInfo.message === 'Usuario inactivo') {
      this.openSnackBar('Usuario debe verificar cuenta', 'OK');
      this.loginForm.reset();
      this.isLoading = false;
    } else if (errorInfo.message === 'Correo incorrecto') {
      this.openSnackBar('Credenciales Incorrectas', 'OK');
      this.loginForm.reset();
      this.isLoading = false;
    } else if (errorInfo.message === 'Contraseña incorrecta') {
      this.openSnackBar('Credenciales Incorrectas', 'OK');
      this.loginAttemptsService.getUserIp().subscribe((resp) => { 
        this.loginAttemptsService
          .newLoginAttempt(
            this.loginForm.get('mail')!.value,
            resp.ip,
            `${resp.city} ${resp.region}`,
            resp.country_name
          )
          .subscribe((resp) => {
            console.log(resp);
            this.loginForm.reset();
            this.isLoading = false;
          });
      });
    }
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 700;
    this._snackBar.open(message, action, config);
  }

  notValidField(field: string) {
    return (
      this.loginForm.get(field)?.invalid && this.loginForm.get(field)?.touched
    );
  }
}
